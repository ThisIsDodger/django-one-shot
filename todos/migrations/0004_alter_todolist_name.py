# Generated by Django 5.0.6 on 2024-06-20 23:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("todos", "0003_alter_todolist_name"),
    ]

    operations = [
        migrations.AlterField(
            model_name="todolist",
            name="name",
            field=models.CharField(max_length=100),
        ),
    ]
