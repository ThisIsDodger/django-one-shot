from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.

def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list": todo_list,
    }
    return render(request, "todos/list.html", context)

def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list": todo_list,
        "todo_items": todo_list.items.all(),
    }
    return render(request, "todos/details.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoListForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)

def todo_list_update(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoListForm(instance=todo_list)

    context = {
        "form": form,
        "todo_list": todo_list,
    }
    return render(request, "todos/edit.html", context)

def todo_list_delete(request, id):
    todo_list_delete = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo_list_delete.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")

def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            todo_list.save()
            return redirect("todo_list_detail", id=todo_list.list.id)
    else:
        form = TodoItemForm()

    context = {
        "form": form,
    }

    return render(request, "todos/create_items.html", context)

def todo_item_update(request, id):
    todo_list = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todo_list.list.id)
    else:
        form = TodoItemForm(instance=todo_list)

    context = {
        "form": form,
        "todo_list": todo_list,
    }
    return render(request, "todos/edit_items.html", context)
